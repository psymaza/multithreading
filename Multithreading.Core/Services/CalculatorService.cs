using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace Multithreading.Core.Services
{
    public class CalculatorService
    {
        public int Sum(IEnumerable<int> collection)
            => collection.Sum();
        
        public int SumAsPLinq(IEnumerable<int> collection)
            => collection.AsParallel().Sum();

        public int SumAsParallel(IEnumerable<int> collection, int threadCount)
        {
            var chunks = SplitChunks(collection, threadCount);
            var tasks = chunks.Select(e => Task.Run(() => Sum(e))).ToArray();
            Task.WaitAll(tasks);
            return tasks.Select(t => t.Result).Sum();
        }

        private static IEnumerable<IEnumerable<int>> SplitChunks(IEnumerable<int> collection, int threadCount)
        {
            var chunkLength = (int) Math.Ceiling(collection.Count() / (double) threadCount);
            var chunks = Enumerable.Range(0, threadCount)
                .Select(i => collection.Skip(i * chunkLength).Take(chunkLength));
            return chunks;
        }
    }
}
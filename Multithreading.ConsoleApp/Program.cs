﻿using System;
using System.Diagnostics;
using System.Linq;
using Multithreading.Core.Services;

namespace Multithreading
{
    class Program
    {
        static void Main(string[] args)
        {
            var arraySize = 10000000;
            var intCollection = Enumerable.Repeat(1, arraySize);
            var calculator = new CalculatorService();
            Executor(() => calculator.Sum(intCollection));
            Executor(() => calculator.SumAsPLinq(intCollection));
            Executor(() => calculator.SumAsParallel(intCollection, Environment.ProcessorCount));
        }

        static void Executor(Func<int> func)
        {
            var sw = new Stopwatch();
            sw.Start();
            func();
            sw.Stop();
            Console.WriteLine("Execute time: {0}", sw.Elapsed.Milliseconds);
        }
    }
}